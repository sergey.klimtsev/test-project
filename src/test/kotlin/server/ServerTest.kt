package server

import com.google.gson.Gson
import io.kotest.core.spec.style.StringSpec
import io.kotest.matchers.shouldBe
import io.ktor.http.HttpMethod
import io.ktor.server.testing.handleRequest
import io.ktor.server.testing.withTestApplication
import server.dto.ResponseDTO
import java.text.SimpleDateFormat
import java.util.*

private val DATE_PATTERN = "yyyy-MM-dd HH:mmX"

class ServerTest : StringSpec({
    "Response ok"{
        withTestApplication({ echo() }) {
            // given
            val formatter = SimpleDateFormat(DATE_PATTERN)
            val payload = "test-string"
            // call
            handleRequest(HttpMethod.Get, "/echo/$payload").apply {
                val response: ResponseDTO = Gson().fromJson(response.content, ResponseDTO::class.java)

                // then
                response.text shouldBe payload
                formatter.format(response.called) shouldBe formatter.format(Date())
            }
        }
    }
})