package server

import io.ktor.application.Application
import io.ktor.application.call
import io.ktor.application.install
import io.ktor.features.CallLogging
import io.ktor.features.Compression
import io.ktor.features.ContentNegotiation
import io.ktor.features.DefaultHeaders
import io.ktor.gson.gson
import io.ktor.response.respond
import io.ktor.routing.get
import io.ktor.routing.routing
import org.slf4j.event.Level
import server.dto.ResponseDTO
import java.util.*

val TEXT_PARAM = "TEXT"
val ISO_DATE_PATTERN = "yyyy-MM-dd'T'HH:mm:ssX"

private fun conf(app: Application) {
    app.install(DefaultHeaders)
    app.install(Compression)
    app.install(CallLogging) {
        level = Level.INFO
    }
    app.install(ContentNegotiation) {
        gson {
            setDateFormat(ISO_DATE_PATTERN)
            setPrettyPrinting()
        }
    }
}

fun Application.echo() {
    conf(this)
    routing {
        get("/echo/{$TEXT_PARAM}") {
            call.respond(ResponseDTO(Date(), call.parameters[TEXT_PARAM] ?: ""))
        }
    }
}

