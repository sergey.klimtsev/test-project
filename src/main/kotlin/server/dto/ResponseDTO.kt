package server.dto

import java.util.*

data class ResponseDTO(val called: Date = Date(), val text: String = "")