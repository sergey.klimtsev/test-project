plugins {
    application
    kotlin("jvm") version "1.3.21"
}

application {
    mainClassName = "server.AppKt"
}


val ktorVersion = "1.3.1"
val kotestVersion = "4.0.0"
val logbackVersion = "1.2.3"

dependencies {
    implementation(kotlin("stdlib"))
    implementation("io.ktor:ktor-server-netty:$ktorVersion")
    implementation("io.ktor:ktor-gson:$ktorVersion")
    implementation("ch.qos.logback:logback-classic:$logbackVersion")
    implementation("io.ktor:ktor-client-cio:$ktorVersion")
    testImplementation("io.ktor:ktor-server-tests:$ktorVersion")
    testImplementation("io.kotest:kotest-runner-junit5-jvm:$kotestVersion")
    testImplementation("io.kotest:kotest-assertions-core-jvm:$kotestVersion")
}

repositories {
    jcenter()
    mavenCentral()
    google()
}

tasks.withType<Test> {
    useJUnitPlatform()
}
